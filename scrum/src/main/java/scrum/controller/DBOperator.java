package scrum.controller;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import scrum.entities.Game;
import scrum.model.CurrentGame;


//@SuppressWarnings("serial")
@ManagedBean
@SessionScoped
public class DBOperator implements Serializable{
	private static final long serialVersionUID = 1L;
	private Session session;
	private SessionFactory factory;
	private Game game;
	
	public DBOperator() {
		game = new Game();
	}
	
	public DBOperator(String config) {
		factory = new Configuration().configure(config).buildSessionFactory();
		session = factory.openSession();
		game = new Game();
	}
	
	@PostConstruct
	public void init(){
		factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		session = factory.openSession();
		game = new Game();
	}
	
	@SuppressWarnings("unchecked")
	public List<Game> getRanking(){
		session.beginTransaction();
		List<Game> ranking = session.createQuery("from Game ORDER BY points DESC").list();
		session.getTransaction().commit();
		int rank = 1;
		for (int i = 0; i < ranking.size(); i++) {
			ranking.get(i).setRank(rank);
			if (i < ranking.size()-1 && ranking.get(i).getPoints() != ranking.get(i+1).getPoints()){
				rank++;
			}
		} 
		return ranking;		
	}
	
	public String saveGame(CurrentGame currentGame){
		game.setLevel(currentGame.getCurrentLevel().getNumberOfLevel());
		game.setPoints(currentGame.getTotalPoints());
		game.setDate(new Date(java.lang.System.currentTimeMillis()));
		session.beginTransaction();
		session.save(game);
		session.getTransaction().commit();
		game = new Game();
		return "ranking"+GameController.REDIRECT;
	}

	public Session getSession() {
		return session;
	}

	public Game getGame() {
		return game;
	}
	
}
