package scrum.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import scrum.model.Block;
import scrum.model.CurrentGame;
import scrum.model.Level;
import scrum.services.BlockMover;
import scrum.services.BlockRemover;
import scrum.services.BoardSetter;
import scrum.services.CountDown;
import scrum.services.GameStatusUpdater;

@ManagedBean
@SessionScoped
public class GameController implements Serializable{
	private static final long serialVersionUID = 1L;
	static final String REDIRECT = "?faces-redirect=true";	
	
	private CurrentGame game;
	private BoardSetter boardSetter;
	private boolean levelGoalPopupVisible;
	
	public GameController() {
		boardSetter = new BoardSetter();
	}
	
	@PostConstruct
	public void init(){
		boardSetter = new BoardSetter();
	}
	
	public String startNewGame(){
		this.game = new CurrentGame();
		this.game.setBoard(boardSetter.initializeBoardfor(game.getCurrentLevel()));
		levelGoalPopupVisible = true;
		return "game"+REDIRECT;
	}
	
	public String toIndex(){
		return "start" + REDIRECT;
	}
	
	public String cancel(){
		return "finalResult" + REDIRECT;
	}
	
	public String tosaveGame(){
		return "saveGame" + REDIRECT;
	}
	
	public String toResult(){
		if(this.game.isWinner() && !this.game.getCurrentLevel().isLastLevel()){
			return "levelResult"+REDIRECT;
		}
		return "finalResult"+REDIRECT;
	}
	
	public void onClick(Block block){		
		if(!this.game.getTime().isOver() && block.isClickable()){
			GameStatusUpdater statusUpdater = new GameStatusUpdater(game);
			BlockRemover remover = new BlockRemover();
			BlockMover mover = new BlockMover(this.game.getBoard());
			int removedBlocks = remover.removeSimilarNeighboursOf(block);
			mover.moveUpBlocks();
			boardSetter.refillBlocks();
			statusUpdater.updatePoints(removedBlocks);
			if(!checkPossibleMoves()){
				statusUpdater.finishGame();
				openDialog("gameFinished");
			}
		}		
	}
	
	private void openDialog(String dialog){
		RequestContext.getCurrentInstance().execute(dialog+".show();");
	}
	
	public void finishLevelOnTimeOut(){
		if(this.game.getTime().isOver()){
			GameStatusUpdater statusUpdater = new GameStatusUpdater(game);
			statusUpdater.finishGame();
			openDialog("gameFinished");
		}
		
	}
	
	private boolean checkPossibleMoves(){
		for (Block block : this.game.getBoard().getBlockTable()) {
			if(block.isClickable()){
				return true;
			}
		}
		return false;
	}
	
	public String startNewLevel(){
		int levelnumber = game.getCurrentLevel().getNumberOfLevel()+1;
		if (levelnumber <= Level.values().length) {
			game.setCurrentLevel(Level.valueOf("LEVEL" + levelnumber));
			this.game.setTime(new CountDown(this.game.getCurrentLevel().getTime()));
			this.game.setCurrentPoints(0);
			this.game.setFinished(false);
			this.game.setBoard(boardSetter.initializeBoardfor(game.getCurrentLevel()));
			levelGoalPopupVisible = true;
			return "game"+REDIRECT;
		}
		return "";
	}	
	
	public void hideLevelGoalPopup(){
		this.levelGoalPopupVisible = false;
	}

	public CurrentGame getGame() {
		return game;
	}

	public void setGame(CurrentGame game) {
		this.game = game;
	}

	public boolean isLevelGoalPopupVisible() {
		return levelGoalPopupVisible;
	}

	public void setLevelGoalPopupVisible(boolean levelGoalPopupVisible) {
		this.levelGoalPopupVisible = levelGoalPopupVisible;
	}

}
