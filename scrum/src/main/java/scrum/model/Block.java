package scrum.model;

import java.util.ArrayList;
import java.util.List;

public class Block {
	public static final int TOP = 0;
	public static final int RIGHT = 1;
	public static final int BOTTOM = 2;
	public static final int LEFT = 3;
	
	private Colour colour;
	private List<Block> neighbours;
	
	public Block(Colour colour){
		this.colour = colour;
		initNeighbours();
	}
	
	private void initNeighbours(){
		this.neighbours = new ArrayList<Block>();
		for (int i = 0; i < 4; i++) {
			this.neighbours.add(null);
		}		
	}
	
	public void clearNeighbours(){
		for (int i = 0; i < 4; i++) {
			this.neighbours.set(i, null);
		}
	}
	
	public boolean isClickable(){
		if(colour == Colour.TRANSPARENT){
			return false;
		}
		for (Block neighbour : neighbours) {
			if (neighbour != null && colour == neighbour.colour){
				return true;
			}				
		}
		return false;
	}
	

	public Colour getColour() {
		return colour;
	}

	public List<Block> getNeighbours() {
		return neighbours;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}
	
	
}
