package scrum.model;

import java.util.List;


public class Board {
	public static final  int BOARDWIDTH = 20;
	public static final int BOARDLENGTH = BOARDWIDTH*BOARDWIDTH;
	public static final int TOTOP = -Board.BOARDWIDTH;
	public static final int TORIGHT = 1;
	public static final int TOBOTTOM = Board.BOARDWIDTH;
	public static final int TOLEFT = -1;
	
	private List<Block> blockTable;
	
	public Board() {
		
	}
	
	
// *********************************************************	
//	Getters and Setters
	public List<Block> getBlockTable() {
		return blockTable;
	}

	public void setBlockTable(List<Block> blockTable) {
		this.blockTable = blockTable;
	}
	
	
	
}
