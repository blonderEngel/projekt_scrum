package scrum.model;

public enum Colour {
	RED(),
	YELLOW(),
	BLUE(),
	GREEN(),
	ORANGE(),
	TRANSPARENT();
	
	public String getStyle(){
		return this.name().toLowerCase();	
	}
}
