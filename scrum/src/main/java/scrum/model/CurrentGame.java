package scrum.model;

import scrum.services.CountDown;


public class CurrentGame {	
	private int currentPoints;
	private int totalPoints;
	private Level currentLevel;
	private Board board;
	private CountDown time;
	private int currentNumberOfBlocks;
	private boolean finished;
	
	public CurrentGame() {
		this.currentPoints = 0;
		this.totalPoints = 0;
		this.finished = false;
		this.currentLevel = Level.LEVEL1;			
		this.time = new CountDown(this.currentLevel.getTime());
	}	
	
	public boolean isWinner(){
		if(this.currentPoints>=this.currentLevel.getGoal()){
			return true;
		}
		return false;
	}
	
	
//	**********************************************
//	Getters and Setters

	public int getCurrentPoints() {
		return currentPoints;
	}

	public void setCurrentPoints(int currentPoints) {
		this.currentPoints = currentPoints;
	}

	public CountDown getTime() {
		return time;
	}

	public void setTime(CountDown time) {
		this.time = time;
	}

	public int getCurrentNumberOfBlocks() {
		return currentNumberOfBlocks;
	}

	public void setCurrentNumberOfBlocks(int currentNumberOfBlocks) {
		this.currentNumberOfBlocks = currentNumberOfBlocks;
	}

	public Level getCurrentLevel() {
		return currentLevel;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}


	public boolean isFinished() {
		return finished;
	}


	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public void setCurrentLevel(Level level) {
		this.currentLevel = level;
	}
	

}
