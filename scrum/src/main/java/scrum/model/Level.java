package scrum.model;

public enum Level {
	LEVEL1(3, 60, 1200),
	LEVEL2(3, 60, 1600),
	LEVEL3(4, 100, 1000),
	LEVEL4(4, 100, 1500),
	LEVEL5(5, 90, 1000);
	
	private int colorCount;
	private int time;
	private int goal;
	
	private Level(int colorCount, int time, int goal) {
		this.colorCount = colorCount;
		this.time = time;
		this.goal = goal;
	}
	
	public int getNumberOfLevel() {
		return Integer.parseInt(this.name().substring(5));
	}
	
	public boolean isLastLevel(){
		if(this.getNumberOfLevel()==Level.values().length){
			return true;
		}
		return false;
	}

	public int getColorCount() {
		return colorCount;
	}

	public int getTime() {
		return time;
	}

	public int getGoal() {
		return goal;
	}
}
