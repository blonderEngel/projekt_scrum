package scrum.services;

import scrum.model.Block;
import scrum.model.Board;
import scrum.model.Colour;

public class BlockMover {
	Board board;
	
	public BlockMover(Board board) {
		this.board = board;
	}
	
	public void moveUpBlocks(){
		int start = board.getBlockTable().size()-Board.BOARDWIDTH;
		for (int i = start; i < board.getBlockTable().size(); i++) {
			int target = i;
			int source = i + Board.TOTOP;
			while (true) {
				Block blockTarget = board.getBlockTable().get(target);
				Block blockSource = board.getBlockTable().get(source);
				if(blockTarget.getColour() == Colour.TRANSPARENT & 
						blockSource.getColour() != Colour.TRANSPARENT){
					blockTarget.setColour(blockSource.getColour());
					blockSource.setColour(Colour.TRANSPARENT);					
				}
				if(blockTarget.getColour() != Colour.TRANSPARENT | 
						blockSource.getColour() != Colour.TRANSPARENT){
					target = target + Board.TOTOP;
				}
				if(source < Board.BOARDWIDTH | target < Board.BOARDWIDTH){
					break;
				}
				source = source + Board.TOTOP;
			}			
		}
	}

}
