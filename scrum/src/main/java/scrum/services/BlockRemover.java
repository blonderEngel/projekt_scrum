package scrum.services;

import scrum.model.Block;
import scrum.model.Colour;

public class BlockRemover {
	
	public int removeSimilarNeighboursOf(Block block){
		Colour colour = block.getColour();
		int count = 0;
		for (Block neighbour : block.getNeighbours()) {
			if (neighbour!=null && !neighbour.getColour().equals(Colour.TRANSPARENT) && colour.equals(neighbour.getColour())){
				count += remove(block);
				count += removeSimilarNeighboursOf(neighbour);	
				count += remove(neighbour);
			}							
		}
		return count;
	}
	
	private int remove(Block block){
		if(block.getColour() != Colour.TRANSPARENT){
			block.setColour(Colour.TRANSPARENT);
			return 1;
		}
		return 0;
	}

}
