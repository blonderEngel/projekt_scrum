package scrum.services;

import java.util.List;

import scrum.model.Block;
import scrum.model.Board;

public class BlockSetter {
	
	public void setNeighboursForBlock(int index, List<Block> blockTable){
		Block block = blockTable.get(index);
		List<Block> neighbours = block.getNeighbours();
		block.clearNeighbours();
		if(index >= Board.BOARDWIDTH){
			neighbours.set(Block.TOP, blockTable.get(index+Board.TOTOP));
		}
		if(index < (Board.BOARDLENGTH-Board.BOARDWIDTH)){
			neighbours.set(Block.BOTTOM, blockTable.get(index+Board.TOBOTTOM));
		}
		if(index%(Board.BOARDWIDTH) != 0){
			neighbours.set(Block.LEFT, blockTable.get(index+Board.TOLEFT));
		}
		if(index%(Board.BOARDWIDTH) != (Board.BOARDWIDTH-1)){
			neighbours.set(Block.RIGHT, blockTable.get(index+Board.TORIGHT));
		}		
	}

}
