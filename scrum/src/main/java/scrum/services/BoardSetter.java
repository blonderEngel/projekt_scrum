package scrum.services;

import java.util.ArrayList;
import java.util.List;

import scrum.model.Block;
import scrum.model.Board;
import scrum.model.Colour;
import scrum.model.Level;

public class BoardSetter {	
	private Board board;
	private ColourRandomizer colourRandomizer;
	
	public BoardSetter() {		
	}
	
	public Board initializeBoardfor(Level level){
		board = new Board();
		colourRandomizer = new ColourRandomizer(level);
		board.setBlockTable(createBlockTable());
		calcNeighbours();
		return board;
	}
	
	private List<Block> createBlockTable(){
		List<Block> blockTable = new ArrayList<Block>();
		for (int i = 0; i < Board.BOARDLENGTH; i++) {
			blockTable.add(new Block(colourRandomizer.getRandomColour()));
		}	
		return blockTable;
	}
	
	private void calcNeighbours(){
		BlockSetter blockSetter = new BlockSetter();
		for (int i = 0; i < board.getBlockTable().size(); i++) {
			blockSetter.setNeighboursForBlock(i, board.getBlockTable());
		}
	}
	
	public void refillBlocks(){
		for (Block block : board.getBlockTable()) {
			if(block.getColour() == Colour.TRANSPARENT){
				block.setColour(colourRandomizer.getRandomColour());
			}
		}
	}

}
