package scrum.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import scrum.model.Colour;
import scrum.model.Level;

public class ColourRandomizer {
	private List<Colour> colourPool = new ArrayList<Colour>();
	
	public ColourRandomizer(Level level) {
		setColourPool(level);
	}
	
	private void setColourPool(Level level){
		Colour[] colours = Colour.values();
		for (int i = 0; i < level.getColorCount(); i++) {
			this.colourPool.add(colours[i]);
		}
	}

	public Colour getRandomColour(){	
		Random randomNumber = new Random();
		int lenghtPool = this.colourPool.size();	
		return colourPool.get(randomNumber.nextInt(lenghtPool));
	}

}
