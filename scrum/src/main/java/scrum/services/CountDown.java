package scrum.services;

public class CountDown {
	private int minutes;
	private int seconds;
	
	public CountDown(int time) {
		this.minutes = Math.round(time/60);
		this.seconds = time%60;
	}
	
	public void decrement(){
		if(isOver()){
			return;
		}
		this.seconds--;
		if(this.seconds == -1){
			this.seconds = 59;
			this.minutes--;
		}
	}
	
	public String getCountDown(){
		String seperator = ":";
		if(this.seconds<10){
			seperator = ":0";
		}
		return String.valueOf(this.minutes) + seperator + String.valueOf(this.seconds);
	}
	
	public boolean isOver(){
		if(this.seconds==0 && this.minutes==0){
			return true;
		}
		return false;
	}
	
	public int getMinutes() {
		return minutes;
	}
	public int getSeconds() {
		return seconds;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
}
