package scrum.services;

import scrum.model.CurrentGame;

public class GameStatusUpdater {
	private static final int POINTSPERBLOCK = 3;
	private CurrentGame game;
	
	public GameStatusUpdater(CurrentGame game){
		this.game = game;
	}

	public void updatePoints(int removedBlocks){
		int points = game.getCurrentPoints();
		game.setCurrentPoints(points+removedBlocks*POINTSPERBLOCK);
	}
	
	public void finishGame(){
		this.game.setFinished(true);
		updateTotalPoints();
	}
	
	private void updateTotalPoints(){
		if(this.game.isWinner()){
			this.game.setTotalPoints(this.game.getTotalPoints()+this.game.getCurrentPoints());
		}
	}
}
