package scrum;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.Board;
import scrum.model.Colour;
import scrum.model.Level;
import scrum.services.BlockMover;
import scrum.services.BoardSetter;

public class BlockMoverTest {
	BlockMover tester;
	Board board;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		BoardSetter setter = new BoardSetter();
		board = setter.initializeBoardfor(Level.LEVEL1);
		tester = new BlockMover(board);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMoveUpOneBlock() {
		int index = 83;
		List<Colour> initialColumn = new ArrayList<Colour>();
		for (int i = 3; i < board.getBlockTable().size(); i+=Board.BOARDWIDTH) {
			initialColumn.add(board.getBlockTable().get(i).getColour());
		}		
		
		board.getBlockTable().get(index).setColour(Colour.TRANSPARENT);
		tester.moveUpBlocks();
		assertEquals(initialColumn.get(3), board.getBlockTable().get(index).getColour());
		assertEquals(initialColumn.get(2), board.getBlockTable().get(index-Board.BOARDWIDTH).getColour());
		assertEquals(initialColumn.get(1), board.getBlockTable().get(index-2*Board.BOARDWIDTH).getColour());
		assertEquals(Colour.TRANSPARENT, board.getBlockTable().get(3).getColour());
	}
	
	@Test
	public void testMoveUpTwoBlocks() {
		int index = 103;
		List<Colour> initialColumn = new ArrayList<Colour>();
		for (int i = 3; i < board.getBlockTable().size(); i+=Board.BOARDWIDTH) {
			initialColumn.add(board.getBlockTable().get(i).getColour());
		}		
		
		board.getBlockTable().get(index).setColour(Colour.TRANSPARENT);
		board.getBlockTable().get(index-Board.BOARDWIDTH).setColour(Colour.TRANSPARENT);
		tester.moveUpBlocks();
		
		assertEquals(initialColumn.get(3), board.getBlockTable().get(index).getColour());
		assertEquals(initialColumn.get(2), board.getBlockTable().get(index-Board.BOARDWIDTH).getColour());
		assertEquals(initialColumn.get(1), board.getBlockTable().get(index-2*Board.BOARDWIDTH).getColour());
		assertEquals(Colour.TRANSPARENT, board.getBlockTable().get(3).getColour());
		assertEquals(Colour.TRANSPARENT, board.getBlockTable().get(23).getColour());
	}

}
