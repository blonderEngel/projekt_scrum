package scrum;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.Block;
import scrum.model.Colour;
import scrum.services.BlockRemover;

public class BlockRemoverTest {
	static private BlockRemover remover;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		remover = new BlockRemover();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	 public void testRemoveBlockGroupsWithNull() {
	  Block origin = new Block(Colour.RED);
	  List<Block> neighbours = origin.getNeighbours();
	  remover.removeSimilarNeighboursOf(origin);
	  assertEquals(Colour.RED, origin.getColour());
	  assertNull(neighbours.get(0));
	  assertNull(neighbours.get(1));
	  assertNull(neighbours.get(2));
	  assertNull(neighbours.get(3));
	 }
	 
	 @Test
	 public void testRemoveBlockGroupsNoMatching() {
	  Block origin = new Block(Colour.RED);
	  List<Block> neighbours = origin.getNeighbours();  
	  neighbours.set(0, new Block(Colour.GREEN));
	  neighbours.set(1, new Block(Colour.YELLOW));
	  neighbours.set(2, new Block(Colour.BLUE));
	  neighbours.set(3, new Block(Colour.TRANSPARENT));  
	  remover.removeSimilarNeighboursOf(origin);
	  assertEquals(Colour.RED, origin.getColour());
	 }
	 
	 @Test
	 public void testRemoveBlockGroupsMatching() {
	  Block origin = new Block(Colour.RED);
	  List<Block> neighbours = origin.getNeighbours();  
	  neighbours.set(0, new Block(Colour.GREEN));
	  neighbours.set(1, new Block(Colour.RED));
	  neighbours.set(2, new Block(Colour.BLUE));
	  neighbours.set(3, new Block(Colour.TRANSPARENT));  
	  remover.removeSimilarNeighboursOf(origin);
	  assertEquals(Colour.TRANSPARENT, origin.getColour());
	  assertEquals(Colour.TRANSPARENT, neighbours.get(1).getColour());
	 }

}
