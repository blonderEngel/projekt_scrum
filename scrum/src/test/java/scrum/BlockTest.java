package scrum;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.Block;
import scrum.model.Colour;

public class BlockTest {
	static Block tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Colour colour = Colour.BLUE;
		tester = new Block(colour);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testBlock() {
		assertEquals("block must have 4 neighbours", 4, tester.getNeighbours().size());
	}
	
	@Test
	public void testClearNeighbours() {
		List<Block> expecteds = new ArrayList<Block>();
		for (int i = 0; i < 4; i++) {
			expecteds.add(null);
		}
		assertArrayEquals("all neighbours must be null", expecteds.toArray(), tester.getNeighbours().toArray());
	}
	
	@Test
	public void testIsClickableOneSameColour(){
		Block middle = new Block(Colour.RED);
		List<Block> neighbours = middle.getNeighbours();		
		neighbours.set(0, new Block(Colour.GREEN));
		neighbours.set(1, new Block(Colour.YELLOW));
		neighbours.set(2, new Block(Colour.BLUE));
		neighbours.set(3, new Block(Colour.RED));
		assertTrue(middle.isClickable());
	}
	
	@Test	
	public void testIsClickableWithoutSameColour(){
		Block middle = new Block(Colour.BLUE);
		List<Block> neighbours1 = middle.getNeighbours();		
		neighbours1.set(0, new Block(Colour.GREEN));
		neighbours1.set(1, new Block(Colour.YELLOW));
		neighbours1.set(2, new Block(Colour.GREEN));
		neighbours1.set(3, new Block(Colour.RED));
		assertFalse(middle.isClickable());
	}
	@Test
	 public void testIsClickableWithoutSameColourWithTransparen(){		
		Block middle = new Block(Colour.YELLOW);
		List<Block> neighbours2 = middle.getNeighbours();		
		neighbours2.set(0, new Block(Colour.GREEN));
		neighbours2.set(1, new Block(Colour.TRANSPARENT));
		neighbours2.set(2, new Block(Colour.GREEN));
		neighbours2.set(3, new Block(Colour.RED));
		assertFalse(middle.isClickable());		
	 }
	 
	 @Test
	 public void testIsClickableMoreThanOneSame(){
		Block middle = new Block(Colour.YELLOW);
		List<Block> neighbours3 = middle.getNeighbours();		
		neighbours3.set(0, new Block(Colour.YELLOW));
		neighbours3.set(1, new Block(Colour.TRANSPARENT));
		neighbours3.set(2, new Block(Colour.YELLOW));
		neighbours3.set(3, new Block(Colour.RED));
		assertTrue(middle.isClickable());
	}

}
