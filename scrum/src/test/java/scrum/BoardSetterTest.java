package scrum;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.Block;
import scrum.model.Board;
import scrum.model.Colour;
import scrum.model.Level;
import scrum.services.BoardSetter;

public class BoardSetterTest {
	static private Board board;
	static private BoardSetter boardSetter;
	static private List<Block> blockTable;
	static private int columns;
	static private int boardlength;
	
	@BeforeClass
	public static void testSetup() {
		boardSetter = new BoardSetter();
		board = boardSetter.initializeBoardfor(Level.LEVEL1);
		blockTable = board.getBlockTable();
		columns = Board.BOARDWIDTH;
		boardlength = Board.BOARDLENGTH;
	}
	
	@AfterClass
	public static void testCleanup() {
	    // Teardown for data used by the unit tests
	}

	@Test
	public void testCalcNeighboursForUpperLeft() {	
		assertNull("Nachbar oben für erstes Element muss Null sein", blockTable.get(0).getNeighbours().get(0));
		assertNotNull("Nachbar rechts für erstes Element darf nicht Null sein", blockTable.get(0).getNeighbours().get(1));
		assertNotNull("Nachbar unten für erstes Element darf nicht Null sein", blockTable.get(0).getNeighbours().get(2));
		assertNull("Nachbar links für erstes Element muss Null sein", blockTable.get(0).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForUpperMiddle() {
		assertNull("Nachbar oben für zweites Element muss Null sein", blockTable.get(1).getNeighbours().get(0));
		assertNotNull("Nachbar rechts für zweites Element darf nicht Null sein", blockTable.get(1).getNeighbours().get(1));
		assertNotNull("Nachbar unten für zweites Element darf nicht Null sein", blockTable.get(1).getNeighbours().get(2));
		assertNotNull("Nachbar links für zweites Element muss Null sein", blockTable.get(1).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForUpperRight() {
		assertNull("Nachbar oben für rechtes oberes Element muss Null sein", blockTable.get(columns-1).getNeighbours().get(0));
		assertNull("Nachbar rechts für rechtes oberes Element darf nicht Null sein", blockTable.get(columns-1).getNeighbours().get(1));
		assertNotNull("Nachbar unten für rechtes oberes Element darf nicht Null sein", blockTable.get(columns-1).getNeighbours().get(2));
		assertNotNull("Nachbar links für rechtes oberes Element muss Null sein", blockTable.get(columns-1).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForRight() {
		assertNotNull("Nachbar oben für rechtes Randelement darf nicht Null sein", blockTable.get(columns*3-1).getNeighbours().get(0));
		assertNull("Nachbar rechts für rechtes Randelement muss Null sein", blockTable.get(columns*3-1).getNeighbours().get(1));
		assertNotNull("Nachbar unten für rechtes Randelement darf nicht Null sein", blockTable.get(columns*3-1).getNeighbours().get(2));
		assertNotNull("Nachbar links für rechtes Randelement darf Null sein", blockTable.get(columns*3-1).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForLeft() {
		assertNotNull("Nachbar oben für linkes Randelement darf nicht Null sein", blockTable.get(columns*3).getNeighbours().get(0));
		assertNotNull("Nachbar rechts für linkes Randelement darf nicht Null sein", blockTable.get(columns*3).getNeighbours().get(1));
		assertNotNull("Nachbar unten für linkes Randelement darf nicht Null sein", blockTable.get(columns*3).getNeighbours().get(2));
		assertNull("Nachbar links für linkes Randelement muss Null sein", blockTable.get(columns*3).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForMiddleCross() {
		assertNotNull("Nachbar oben für mittleres Element an der Diagonalen darf nicht Null sein", blockTable.get((columns-1)*3).getNeighbours().get(0));
		assertNotNull("Nachbar rechts für mittleres Element an der Diagonalen darf nicht Null sein", blockTable.get((columns-1)*3).getNeighbours().get(1));
		assertNotNull("Nachbar unten für mittleres Element an der Diagonalen darf nicht Null sein", blockTable.get((columns-1)*3).getNeighbours().get(2));
		assertNotNull("Nachbar links für mittleres Element an der Diagonalen darf nicht Null sein", blockTable.get((columns-1)*3).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForBottomLeft() {		
		assertNotNull("Nachbar oben für linkes unteres Element darf nicht Null sein", blockTable.get(boardlength-columns).getNeighbours().get(0));
		assertNotNull("Nachbar rechts für linkes unteres Element darf nicht Null sein", blockTable.get(boardlength-columns).getNeighbours().get(1));
		assertNull("Nachbar unten für linkes unteres Element muss Null sein", blockTable.get(boardlength-columns).getNeighbours().get(2));
		assertNull("Nachbar links für linkes unteres Element muss Null sein", blockTable.get(boardlength-columns).getNeighbours().get(3));
	}
	
	@Test
	public void testCalcNeighboursForBottomRight() {
		assertNotNull("Nachbar oben für letztes Element muss Null sein", blockTable.get(boardlength-1).getNeighbours().get(0));
		assertNull("Nachbar rechts für letztes Element muss Null sein", blockTable.get(boardlength-1).getNeighbours().get(1));
		assertNull("Nachbar unten für letztes Element muss nicht Null sein", blockTable.get(boardlength-1).getNeighbours().get(2));
		assertNotNull("Nachbar links für letztes Element darf nicht Null sein", blockTable.get(boardlength-1).getNeighbours().get(3));
	}
	
	@Test
	public void testRefillBoard(){		
		blockTable.get(10).setColour(Colour.TRANSPARENT);
		blockTable.get(20).setColour(Colour.TRANSPARENT);
		blockTable.get(55).setColour(Colour.TRANSPARENT);
		boardSetter.refillBlocks();
		for (Block block : blockTable) {
			assertNotSame(Colour.TRANSPARENT, block.getColour());
		}
	}

}
