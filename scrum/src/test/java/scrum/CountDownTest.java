package scrum;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.services.CountDown;

public class CountDownTest {
	private CountDown tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		tester = null;
	}

	@Test
	public void testDecrementStop() {
		tester = new CountDown(0);
		tester.decrement();
		assertEquals(0, tester.getMinutes());
		assertEquals(0, tester.getSeconds());
	}
	
	@Test
	public void testDecrementMinutes() {
		tester = new CountDown(120);
		tester.decrement();
		assertEquals(1, tester.getMinutes());
		assertEquals(59, tester.getSeconds());
	}
	
	@Test
	public void testDecrementSeconds() {
		tester = new CountDown(165);
		tester.decrement();
		assertEquals(2, tester.getMinutes());
		assertEquals(44, tester.getSeconds());
	}

	@Test
	public void testGetCountDownSecondsLower10() {
		tester = new CountDown(126);
		assertEquals("2:06", tester.getCountDown());
	}
	
	@Test
	public void testGetCountDownSecondsHigher10() {
		tester = new CountDown(143);
		assertEquals("2:23", tester.getCountDown());
	}

}
