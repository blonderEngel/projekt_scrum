package scrum;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.CurrentGame;

public class CurrentGameTest {
	CurrentGame tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		tester = new CurrentGame();
	}

	@After
	public void tearDown() throws Exception {
		tester = null;
	}

	@Test
	public void testIsWinner() {
		tester.setCurrentPoints(1200);
		assertTrue(tester.isWinner());
	}
	
	@Test
	public void testIsLoser() {
		tester.setCurrentPoints(800);
		assertFalse(tester.isWinner());
	}

}
