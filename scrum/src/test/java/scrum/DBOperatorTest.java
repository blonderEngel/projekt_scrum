package scrum;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import scrum.controller.DBOperator;
import scrum.entities.Game;
import scrum.model.CurrentGame;
import scrum.model.Level;

public class DBOperatorTest {
	DBOperator tester;
	SessionFactory factory;
	
	@Before
	public void setUp() throws Exception {
		tester = new DBOperator("hibernateTest.cfg.xml");
	}

	@After
	public void tearDown() throws Exception {
		tester.getSession().beginTransaction();
		tester.getSession().createQuery("delete from Game").executeUpdate();
		tester.getSession().getTransaction().commit();
		tester = null;
	}

	@Test
	public void testGetRanking() {
		tester.getSession().beginTransaction();
		tester.getSession().save(new Game("Dummy1", 1000, 1, new Date(456879)));
		tester.getSession().save(new Game("Dummy2", 1000, 1, new Date(456879)));
		tester.getSession().save(new Game("Dummy3", 1500, 1, new Date(456879)));
		tester.getSession().getTransaction().commit();
		List<Game> ranking = tester.getRanking();
		assertEquals("Dummy3", ranking.get(0).getUser());
		assertEquals(1, ranking.get(0).getRank());
		assertEquals("Dummy1", ranking.get(1).getUser());
		assertEquals(2, ranking.get(1).getRank());
		assertEquals("Dummy2", ranking.get(2).getUser());
		assertEquals(2, ranking.get(2).getRank());
	}
	
	@Test
	public void testSaveGame(){
		tester.getGame().setUser("test");
		Game gameToSave = tester.getGame();
		CurrentGame currentGame = new CurrentGame();
		currentGame.setCurrentLevel(Level.LEVEL1);
		currentGame.setTotalPoints(1500);
		tester.saveGame(currentGame);
		assertNotSame(gameToSave, tester.getGame());
		assertTrue(tester.getSession().contains(gameToSave));
	}

}
