package scrum;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.controller.GameController;
import scrum.model.Board;
import scrum.model.CurrentGame;
import scrum.model.Level;
import scrum.services.CountDown;

public class GameControllerTest {
	GameController tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		tester = new GameController();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNewGame() {
		tester.startNewGame();
		CurrentGame gameCopy = tester.getGame();
		Board boardCopy = tester.getGame().getBoard();
		tester.startNewGame();
		assertNotSame(gameCopy, tester.getGame());
		assertNotSame(boardCopy, tester.getGame().getBoard());

	}
	
	public void TestStartNewLevel1To2(){
		CurrentGame game = tester.getGame();
		game.setCurrentPoints(1050);
		game.setTime(new CountDown(0));
		game.setFinished(true);
		Board board = game.getBoard();
		tester.startNewLevel();
		assertEquals(0, game.getCurrentPoints());
		assertEquals(2, game.getTime().getMinutes());
		assertEquals(0, game.getTime().getSeconds());
		assertFalse(game.isFinished());
		assertEquals(Level.LEVEL2, game.getCurrentLevel());
		assertNotSame(board, game.getBoard());	
		
	}
	
	public void TestStartNewLevel5toNothing(){
		CurrentGame game = tester.getGame();
		game.setCurrentLevel(Level.LEVEL5);
		game.setCurrentPoints(1050);
		game.setTime(new CountDown(0));
		game.setFinished(true);
		Board board = game.getBoard();
		tester.startNewLevel();
		assertEquals(1050, game.getCurrentPoints());
		assertEquals(0, game.getTime().getMinutes());
		assertEquals(0, game.getTime().getSeconds());
		assertTrue(game.isFinished());
		assertEquals(Level.LEVEL5, game.getCurrentLevel());
		assertEquals(board, game.getBoard());	
		
	}

}
