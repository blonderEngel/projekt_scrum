package scrum;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import scrum.model.CurrentGame;
import scrum.services.GameStatusUpdater;

public class GameStatusUpdaterTest {
	GameStatusUpdater statusUpdater;
	CurrentGame game;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
		statusUpdater = new GameStatusUpdater(game);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUpdatePoints() {		
		statusUpdater.updatePoints(5);
		assertEquals(15, game.getCurrentPoints());
		statusUpdater.updatePoints(10);
		assertEquals(45, game.getCurrentPoints());
	}
	
	@Test
	public void testUpdateTotalPointsOnWin(){
		game.setCurrentPoints(1200);
		game.setTotalPoints(20);
		statusUpdater.finishGame();
		assertEquals(1220, game.getTotalPoints());
	}
	
	@Test
	public void testUpdateTotalPointsOnLose(){
		game.setCurrentPoints(800);
		game.setTotalPoints(20);
		statusUpdater.finishGame();
		assertEquals(20, game.getTotalPoints());
	}

}
