package scrum;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import scrum.model.Level;

public class LevelTest {
	Level tester;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		tester = null;
	}

	@Test
	public void testGetNumberOfLevel() {
		tester = Level.LEVEL2;
		assertEquals(2, tester.getNumberOfLevel());
	}
	
	@Test
	public void test5IsLastLevel() {
		tester = Level.LEVEL5;
		assertTrue(tester.isLastLevel());
	}
	
	@Test
	public void test3IsNotLastLevel() {
		tester = Level.LEVEL3;
		assertFalse(tester.isLastLevel());
	}
	

}
